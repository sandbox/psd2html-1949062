Description:
================================================================================
Drupal Twitter is a simple-to-use module that allows creation of multiple blocks
with Twitter feeds.
Each block has independent settings and pulls results from Twitter with the help
of public queries or the Twitter REST API. (The OAuth module available at
http://drupal.org/project/oauth is required for this.)
The style of the Twitter feed is fully customizable using the
drupal-twitter-list.tpl.php template.


Installation:
================================================================================
1. Download and install the module as usual. 
2. Download and install the Oauth module if use of Twitter API is necessary.
Go to Administration » Configuration » User interface » Twitter, and configure
your access keys there. 
3. Go to Administration » Structure » Blocks, and create a new Twitter block
using the “Add twitter block” link. 
4. Configure the block settings with desired Twitter parameters using the
on-screen instructions.


Features:
================================================================================
- pulls Twitter feed from one or more Twitter user accounts
- pulls Twitter search results by hashtag or search term
- uses public queries and the Twitter API to obtain data
- cache enabled
- user avatars with different sizes
- customizable publish date format including “time ago” format
- Twitter message styling
- shows multiple blocks on one page
