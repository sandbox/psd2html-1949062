<?php
/**
 * @file
 * Template file for the Drupal Twitter module.
 *
 * Available custom variables:
 * - $items: An array of tweets.
 * Each $item in $items is an associative array, which has such structure:
 * $item['id']      --  ID of the tweet
 * $item['tweet']  --  rendered tweet
 * $item['source'] --  tweet source
 * $item['date'] -- tweet post date in format, defined in block settings
 * $item['url'] -- individual tweet url
 * $item['user'] -- user screen_name
 * $item['link'] -- link to user profile
 *
 * - $config: An array of block config options.
 * - $delta: An integer delta number of block.
 * - $show_avatar: Flag
 */
?>
<!-- drupal-twitter-list template -->
<div class="twitter-list">
<ol>
<?php if (count($items)):?>
  <?php foreach ($items as $item): ?>
    <li>
    <?php if ($show_avatar && $item['avatar']):?>
      <img style="display: block;" class="avatar" src="<?php echo $item['avatar']?>" alt="<?php echo $item['user']?>">
    <?php endif;?>
    Author: <?php echo $item['link']; ?><br />
    <?php echo $item['tweet'] ?><br />
    Posted: <?php echo $item['date']?></li>
  <?php endforeach;?>
  <?php else :?>
    <li>No items found</li>
  <?php endif;?>
</ol>
</div>
<!-- /drupal-twitter-list template -->
