<?php

/**
 * @file
 * Admin page callbacks for the module.
 */

/**
 * Implements hook_add_block_form().
 */
function drupal_twitter_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $form = block_admin_configure($form, $form_state, 'drupal_twitter', NULL);

  // Other modules should be able to use hook_form_block_add_block_form_alter()
  // to modify this form, so add a base form ID.
  $form_state['build_info']['base_form_id'] = 'block_add_block_form';

  // Prevent block_add_block_form_validate/submit() from being automatically
  // added because of the base form ID by providing these handlers manually.
  $form['#validate'] = array();
  $form['#submit'] = array('drupal_twitter_add_block_form_submit');

  return $form;
}

/**
 * Implements hook_admin_settings_form().
 */
function drupal_twitter_admin_settings_form($form, &$form_state) {
  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for twitter, you must <a href="@url">register your application</a> with Twitter and add the provided keys here.', array('@url' => 'https://dev.twitter.com/apps/new')),
    '#tree' => TRUE,
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('admin/config/user-interface/drupal-twitter/save', array('absolute' => TRUE)),
  );
  $form['oauth']['drupal_twitter_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('drupal_twitter_consumer_key', NULL),
  );
  $form['oauth']['drupal_twitter_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('drupal_twitter_consumer_secret', NULL),
  );
  $form['oauth']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ajax' => array(
      'callback' => 'drupal_twitter_admin_settings_form_submit',
      'wrapper' => 'oauth',
    ),
    '#submit' => array('drupal_twitter_admin_settings_form_submit'),
  );

  $form['oauth_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Access Settings'),
    '#access' => module_exists('oauth_common'),
    '#tree' => TRUE,
  );
  $form['oauth_access']['drupal_twitter_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Access key'),
    '#default_value' => variable_get('drupal_twitter_access_key', NULL),
  );
  $form['oauth_access']['drupal_twitter_access_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Access secret'),
    '#default_value' => variable_get('drupal_twitter_access_secret', NULL),
  );
  $form['oauth_access']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get access keys'),
    '#submit' => array('drupal_twitter_admin_settings_access_form_submit'),
  );
  $form['oauth_access']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset access keys'),
    '#submit' => array('drupal_twitter_admin_settings_access_form_reset'),
  );
  $form['clear_image_cache'] = array(
    '#access' => module_exists('image'),
    '#type' => 'submit',
    '#value' => t('Clear avatars cache'),
    '#submit' => array('drupal_twitter_flush_image_cache'),
  );
  $form['drupal_twitter_cache_time'] = array(
    '#type' => 'select',
    '#title' => t('Cache lifetime'),
    '#options' => array(
      0  => 'No cache',
      1  => '1 minute',
      5  => '5 minutes',
      10 => '10 minutes',
      15 => '15 minutes',
      30 => '30 minutes',
      60 => '1 hour',
      120 => '2 hour',
    ),
    '#default_value' => variable_get('drupal_twitter_cache_time', 5),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_admin_settings_form_submit().
 */
function drupal_twitter_admin_settings_form_submit($form, &$form_state) {
  if ($vals = $form_state['values']['oauth']) {
    variable_set('drupal_twitter_consumer_key', $vals['drupal_twitter_consumer_key']);
    variable_set('drupal_twitter_consumer_secret', $vals['drupal_twitter_consumer_secret']);
  }
  drupal_set_message('Keys saved');
  $form['oauth']['drupal_twitter_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#value' => $vals['drupal_twitter_consumer_key'],
  );
  $form['oauth']['drupal_twitter_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#value' => $vals['drupal_twitter_consumer_secret'],
  );
  return $form;
}

/**
 * Implements hook_admin_settings_access_form_submit().
 */
function drupal_twitter_admin_settings_access_form_submit($form, &$form_state) {
  if (!extension_loaded('curl')) {
    drupal_set_message(t('Please check that "cURL" PHP Extension is loaded'), 'warning');
    drupal_goto('admin/config');
  }

  // Have to redirect, because of overlay 'bug' when requesting external url.
  if (module_exists('overlay')) {
    $mode = overlay_get_mode();
    if ($mode) {
      drupal_goto('admin/config/user-interface/drupal-twitter', array('fragment' => '', 'query' => array('render' => NULL)));
    }
  }

  // Load required class.
  module_load_include('inc', 'drupal_twitter', 'drupal_twitter.core');
  $token = NULL;

  $vals = $form_state['values'];
  $consumer_key = variable_get('drupal_twitter_consumer_key', NULL);
  $consumer_secret = variable_get('drupal_twitter_consumer_secret', NULL);

  // Requesting for authorization.
  if (isset($consumer_key) && isset($consumer_secret)) {
    $connection = new DrupalTwitterOAuth($consumer_key, $consumer_secret);
    $request_token = $connection->getRequestToken(url('admin/config/user-interface/drupal-twitter/save', array('absolute' => TRUE)));
    if (isset($request_token['oauth_token'])) {
      $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
    }
    if (isset($request_token['oauth_token_secret'])) {
      $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
    }

    switch ($connection->httpCode) {
      case 200:
        /* Build authorize URL and redirect user to Twitter. */
        if ($token) {
          $url = $connection->getAuthorizeURL($token);
          drupal_goto($url);
        }
        break;

      default:
        /* Show notification if something went wrong. */
        drupal_set_message(t('Could not connect to Twitter. Refresh the page or try again later.'));
        break;
    }
  }
}

/**
 * Implements hook_admin_settings_access_form_reset().
 */
function drupal_twitter_admin_settings_access_form_reset($form, &$form_state) {
  variable_del('drupal_twitter_access_key');
  variable_del('drupal_twitter_access_secret');
}

/**
 * Implements hook_admin_settings_form_callback().
 */
function drupal_twitter_admin_settings_form_callback($form, &$form_state) {
  module_load_include('inc', 'drupal_twitter', 'drupal_twitter.core');
  $form_state['values']['access_token'] = '';
  $form_state['values']['access_secret'] = '';
  $consumer_key = variable_get('drupal_twitter_consumer_key', '');
  $consumer_secret = variable_get('drupal_twitter_consumer_secret', '');

  if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
    $_SESSION['oauth_status'] = 'oldtoken';
    drupal_set_message('Problem with oauth token');
  }
  else {
    $connection = new DrupalTwitterOAuth($consumer_key, $consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
    $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

    /* Remove no longer needed request tokens */
    unset($_SESSION['oauth_token']);
    unset($_SESSION['oauth_token_secret']);
    $form_state['values']['access_token'] = $access_token['oauth_token'];
    $form_state['values']['access_secret'] = $access_token['oauth_token_secret'];
  }
  drupal_form_submit('drupal_twitter_admin_settings_form_callback_form', $form_state);
}

/**
 * Implements hook_admin_settings_form_callback_form().
 */
function drupal_twitter_admin_settings_form_callback_form($form, &$form_state) {

  $form['access_token'] = array(
    '#type' => 'hidden',
    '#default_value' => $form_state['values']['access_token'],
  );
  $form['access_secret'] = array(
    '#type' => 'hidden',
    '#default_value' => $form_state['values']['access_secret'],
  );
  return $form;
}

/**
 * Implements hook_admin_settings_form_callback_form_submit().
 */
function drupal_twitter_admin_settings_form_callback_form_submit($form, &$form_state) {
  variable_set('drupal_twitter_access_key', $form_state['values']['access_token']);
  variable_set('drupal_twitter_access_secret', $form_state['values']['access_secret']);
  $form_state['programmed'] = FALSE;
  $form_state['redirect'] = url('admin/config/user-interface/drupal-twitter', array('absolute' => TRUE));
}

/**
 * Implements hook_block_configure().
 */
function drupal_twitter_block_configure($delta = '') {
  $form = array();
  $values = _drupal_twitter_get_config($delta);

  // Build the standard form.
  $form['#attached']['css'][] = drupal_get_path('module', 'drupal_twitter') . '/css/drupal_twitter_admin.css';

  $form['drupal-twitter-wrapper-start'] = array(
    '#markup' => '<div id="drupal-twitter-settings">',
    '#weight' => -15,
  );

  $options = array('rss' => t('RSS'));
  if (module_exists('oauth_common')) {
    $options['api'] = t('Twitter REST API');
  }

  $form['connection_options'] = array(
    '#type' => 'radios',
    '#title' => t('Connection type'),
    '#default_value' => $values['connection_options'],
    '#options' => $options,
    '#attributes' => array('class' => array('clearfix', 'feed-type')),
  );

  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#default_value' => $values['username'],
    '#description' => t('You can specify more than 1 user, separated with ","'),
    '#element_validate' => array('drupal_twitter_add_block_form_validate'),
  );

  $form['hashtag'] = array(
    '#type' => 'textfield',
    '#title' => t('Hashtag'),
    '#default_value' => $values['hashtag'],
    '#description' => t('You can specify more than 1 value, with "#" at start and separated with "," '),
  );

  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search query'),
    '#default_value' => $values['search'],
    '#description' => t('Search string, without "#"'),
  );

  $form['num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of tweets'),
    '#default_value' => $values['num'],
    '#description' => t('Number of tweets to show in feed'),
  );

  $form['date_style'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show date in "time ago" format'),
    '#default_value' => $values['date_style'],
  );
  $form['date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#default_value' => $values['date_format'],
    '#description' => t('More info about formats <a href="@url" target="_blank">here</a>', array('@url' => 'http://php.net/manual/en/function.date.php')),
    '#states' => array(
      'invisible' => array(
        ':input[name="date_style"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['clickable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make links, users and hashcodes clickable'),
    '#default_value' => $values['clickable'],
  );

  if (module_exists('image')) {
    $form['show_avatar'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show avatars'),
      '#default_value' => $values['show_avatar'],
    );

    $form['avatar_style'] = array(
      '#type' => 'select',
      '#title' => t('Avatar image style'),
      '#options' => drupal_map_assoc(image_style_options()),
      '#default_value' => $values['avatar_style'],
      '#states' => array(
        'invisible' => array(
          ':input[name="show_avatar"]' => array('checked' => FALSE),
        ),
      ),
    );
  }

  $form['drupal-twitter-wrapper-close'] = array('#markup' => '</div>');
  return $form;
}

/**
 * Implements hook_add_block_form_validate().
 */
function drupal_twitter_add_block_form_validate(&$elements, &$form_state, $form_id = NULL) {
  if (!empty($elements['#value'])) {
    $status = _drupal_twitter_check_users($elements['#value'], $form_state['values']);
    foreach ($status as $id => $status) {
      if ($status['code'] == 200) {
        continue;
      }
      drupal_set_message($status['message'], 'warning');
      form_set_error($elements['#name'], t('Please check entered values'));
    }
  }
}


/**
 * Implements hook_add_block_form_validate().
 */
function drupal_twitter_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('drupal_twitter_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  variable_set('drupal_twitter_ids', $block_ids);

  // Save the block configuration.
  drupal_twitter_block_save($delta, $form_state['values']);

  // Run the normal new block submission.
  $query = db_insert('block')->fields(array(
                                            'visibility',
                                            'pages',
                                            'custom',
                                            'title',
                                            'module',
                                            'theme',
                                            'region',
                                            'status',
                                            'weight',
                                            'delta',
                                            'cache',
                                            ));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'status' => 0,
        'status' => (int) ($region != BLOCK_REGION_NONE),
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();
  drupal_set_message(t('The block has been created.'));
  $form_state['redirect'] = "admin/structure/block/manage/drupal_twitter/{$delta}/configure";
}

/**
 * Implements hook_block_save().
 */
function drupal_twitter_block_save($delta = '', $edit = array()) {
  if (!empty($delta)) {
    $config = _drupal_twitter_get_config($delta);
    $keys = array_keys($config);
    foreach ($keys as $key) {
      $values[$key] = isset($edit[$key]) ? $edit[$key] : $config[$key];
    }
    cache_clear_all("drupal_twitter_block_{$delta}", 'cache_block');
    variable_set("drupal_twitter_{$delta}_config", $values);
  }
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function drupal_twitter_delete($form, &$form_state, $delta = 0) {
  $title = 'Drupal Twitter #' . $delta;
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Deletion of Twitter Blocks.
 */
function drupal_twitter_delete_submit($form, &$form_state) {
  // Remove the menu block configuration variables.
  $delta = $form_state['values']['delta'];
  $block_ids = variable_get('drupal_twitter_ids', array());
  unset($block_ids[array_search($delta, $block_ids)]);
  sort($block_ids);
  variable_set('drupal_twitter_ids', $block_ids);
  variable_del("drupal_twitter_{$delta}_config");

  db_delete('block')
    ->condition('module', 'drupal_twitter')
    ->condition('delta', $delta)
    ->execute();
  db_delete('block_role')
    ->condition('module', 'drupal_twitter')
    ->condition('delta', $delta)
    ->execute();
  drupal_set_message(t('The block "%name" has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all("drupal_twitter_block_{$delta}", 'cache_block');
  $form_state['redirect'] = 'admin/structure/block';
  return;
}

/**
 * Check user names.
 */
function _drupal_twitter_check_users($users, $values = array()) {
  module_load_include('inc', 'drupal_twitter', 'drupal_twitter.core');
  $users = explode(',', trim($users));

  switch ($values['connection_options']) {
    case 'api':
      module_load_include('rest.inc', 'drupal_twitter');
      $cn = new DrupalTwitterAPI();
      break;

    case 'rss':
    default:
      module_load_include('rss.inc', 'drupal_twitter');
      $cn = new DrupalTwitterRSS($values);
      break;
  }

  foreach ($users as $id => $user) {
    if (empty($user)) {
      continue;
    }
    $status = $cn->checkUser($user);
    $return[$id]['code'] = $status['code'];
    if ($status['code'] == 200) {
      $return[$id]['message'] = t("User <strong>%user</strong> is OK ", array('%user' => $user));
    }
    else {
      $return[$id]['message'] = t("User <strong>%user</strong> message: error with code %status", array('%user' => $user, '%status' => $status['code']));
    }
  }
  return $return;
}
