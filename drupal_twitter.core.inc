<?php

/**
 * @file
 * Admin page callbacks for the module.
 */

class DrupalTwitter {

  public static $profileURL = 'users/profile_image';

  /**
   * Save profile image in local directory.
   */
  public static function saveProfileImage($user, $size = 'original') {
    $query['screen_name'] = $user;
    $query['size'] = $size;
    $url = DRUPAL_TWITTER_API . '/1/' . self::$profileURL;
    $url = url($url, array('query' => $query));
    $respond = drupal_http_request($url);
    if ($respond->code == 200) {
      $f = file_get_contents($respond->redirect_url);
      $info = pathinfo($respond->redirect_url);
      $ext = $info['extension'];
      $fname = strtolower($user) . '.' . $ext;
      $destination = file_destination(file_default_scheme() . '://' . $fname, FILE_EXISTS_REPLACE);
      $file = $destination ? file_save_data($f, $destination, FILE_EXISTS_REPLACE) : NULL;
      if (is_object($file)) {
        $fields = array(
          'screen_name' => $user,
          'fid'     => $file->fid,
        );
        $query = db_insert('drupal_twitter_avatars')
          ->fields($fields)
          ->execute();
        $query = db_select('drupal_twitter_avatars', 'a')
            ->condition('a.screen_name', strtolower($user), '=')
            ->fields('a', array('screen_name', 'fid'))
            ->execute();
        return $query->fetch(PDO::FETCH_OBJ);
      }
    }
    return FALSE;
  }

  /**
   * Check saved profile image for a specified user.
   */
  public static function checkProfileImage($user) {
    $query = db_select('drupal_twitter_avatars', 'a')
            ->condition('a.screen_name', strtolower($user), '=')
            ->fields('a', array('screen_name', 'fid'))
            ->range(0, 1)
            ->execute();
    $item = $query->rowCount() ? $query->fetch(PDO::FETCH_OBJ) : FALSE;
    return $item;
  }

  /**
   * Load profile image if not exists and return.
   */
  public static function getProfileImage($user, $style = 'twitter_avatar') {
    if (!module_exists('image')) {
      return FALSE;
    }

    $img = self::checkProfileImage($user);
    if (!$img) {
      $img = self::saveProfileImage($user);
    }
    $file = file_load($img->fid);
    if (!$file) {
      return FALSE;
    }
    $url = image_style_url($style, $file->uri);
    return $url;
  }
}

class DrupalTwitterItem {

  protected $protocol = 'http://';
  protected $apiURL = 'api.twitter.com/1/';
  protected $searchURL = 'search.twitter.com/search.json?q=';

  protected $params;

  protected $users = NULL;
  protected $tags = NULL;
  protected $search = NULL;


  /**
   * Constructor.
   */
  public function __construct($params = array()) {
    if (!empty($params['username'])) {
      $users = explode(',', trim($params['username']));
      array_walk($users, array($this, 'normalize'));
      $this->users = array_unique($users);
    }
    if (!empty($params['hashtag'])) {
      $tags = explode(',', trim($params['hashtag']));
      $this->tags = $tags;
    }
    if (!empty($params['search'])) {
      $this->search = trim($params['search']);
    }

    $this->params = $params;
  }

  /**
   * Correct user name.
   */
  protected function normalize(&$user) {
    $user = trim($user);
    if (stripos($user, '!') === 0) {
      $user = substr($user, 1);
    }
    if (stripos($user, '|') > 1) {
      $user = substr($user, 0, stripos($user, '|'));
    }
  }

  /**
   * Concatenate tags into query string.
   */
  protected function buildTagsQuery($tags) {
    $tags = array_map('trim', $tags);
    $query = implode(' OR ', $tags);
    return urlencode($query);
  }

  /**
   * Concatenate users into query string.
   */
  protected function buildUsersQuery($users) {
    $users = array_map('urlencode', $users);
    $query = 'from:' . implode(' OR from:', $users);
    return urlencode($query);
  }


  /**
   * Make user profile link.
   */
  protected function linkToProfile($user, $attributes = array()) {
    return '<a ' . drupal_attributes($attributes) . ' href="http://twitter.com/' . $user . '">' . $user . '</a>';
  }

  /**
   * Make link to tweet.
   */
  protected function linkToTweet($user, $id) {
    return "https://twitter.com/{$user}/status/{$id}";
  }


  /**
   * Replace @username with a link to that twitter user.
   * 
   * @param string $text
   *   Tweet text
   * 
   * @return string
   *   Tweet text with @replies linked
   */
  public function linkTwitterUsers($text) {
    $text = preg_replace_callback('/(^|\s)@(\w+)/i', array($this, 'linkTwitterUsersCallback'), $text);
    return $text;
  }

  /**
   * Make user names clickable.
   */
  protected function linkTwitterUsersCallback($matches) {
    $link_attrs = array(
      'href'  => 'http://twitter.com/' . urlencode($matches[2]),
      'class'  => 'twitter-user',
    );
    return $matches[1] . $this->buildLink('@' . $matches[2], $link_attrs);
  }

  /**
   * Replace #hashtag with a link to search.twitter.com for that hashtag.
   *
   * @param string $text
   *   Tweet text
   *
   * @return string
   *   Tweet text with #hashtags linked
   */
  public function linkHashtags($text) {
    $text = preg_replace_callback('/(^|\s)(#[\w\x{00C0}-\x{00D6}\x{00D8}-\x{00F6}\x{00F8}-\x{00FF}]+)/iu', array($this, 'linkHashtagsCallback'), $text);
    return $text;
  }

  /**
   * Replace #hashtag with a link to search.twitter.com for that hashtag.
   *
   * @param array $matches
   *   Tweet text
   *
   * @return string
   *   Tweet text with #hashtags linked
   */
  protected function linkHashtagsCallback($matches) {
    $link_attrs = array(
      'href'  => 'http://search.twitter.com/search?q=' . urlencode($matches[2]),
      'class'  => 'twitter-hashtag',
    );
    return $matches[1] . $this->buildLink($matches[2], $link_attrs);
  }

  /**
   * Turn URLs into links.
   *
   * @param string $text
   *   Tweet text
   *
   * @return string
   *   Tweet text with URLs repalced with links
   */
  public function linkUrls($text) {
    // Pad with whitespace to simplify the regexes.
    $text = " {$text} ";

    $url_clickable = '~
      ([\\s(<.,;:!?])
      (
        [\\w]{1,20}+://
        (?=\S{1,2000}\s)
        [\\w\\x80-\\xff#%\\~/@\\[\\]*(+=&$-]*+
        (?:
          [\'.,;:!?)]
          [\\w\\x80-\\xff#%\\~/@\\[\\]*(+=&$-]++)*
      )
      (\)?)                                                  # 3: Trailing closing parenthesis (for parethesis balancing post processing)
    ~xS';
    // The regex is a non-anchored pattern and does not have a single fixed
    // starting character.
    // Tell PCRE to spend more time optimizing since, when used on a page load,
    // it will probably be used several times.
    $text = preg_replace_callback($url_clickable, array($this, 'makeURLClickableCallback'), $text);

    $text = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]+)#is', array($this, 'makeWebFtpClickableCallback'), $text);
    $text = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', array($this, 'makeEmailClickableCallback'), $text);

    // Remove our whitespace padding.
    $text = substr($text, 1, -1);

    return $text;
  }

  /**
   * Make ftp-links clickable.
   */
  protected function makeWebFtpClickableCallback($matches) {
    $ret = '';
    $dest = $matches[2];
    $dest = url($dest);
    if (empty($dest)) {
      return $matches[0];
    }

    // Removed trailing [.,;:)] from URL.
    if (in_array(substr($dest, -1), array('.', ',', ';', ':', ')')) === TRUE) {
      $ret = substr($dest, -1);
      $dest = substr($dest, 0, strlen($dest) - 1);
    }
    $link_attrs = array(
      'href'  => $dest,
    );
    return $matches[1] . $this->buildLink($dest, $link_attrs) . $ret;
  }

  /**
   * Make emails clickable.
   */
  protected function makeEmailClickableCallback($matches) {
    $email = $matches[2] . '@' . $matches[3];
    $link_attrs = array(
      'href'  => 'mailto:' . $email,
    );
    return $matches[1] . $this->buildLink($email, $link_attrs);
  }

  /**
   * Make url clickable.
   */
  protected function makeURLClickableCallback($matches) {
    $link_attrs = array(
      'href'  => $matches[2],
    );
    return $matches[1] . $this->buildLink($matches[2], $link_attrs);
  }

  /**
   * Check if arg not empty.
   */
  protected function notEmpty($v) {
    return !(empty($v));
  }

  /**
   * Make link.
   */
  protected function buildLink($text, $attributes = array(), $no_filter = FALSE) {
    $attributes = array_filter($attributes, array($this, 'notEmpty'));
    $link = '<a' . drupal_attributes($attributes) . '>' . $text . '</a>';
    return $link;
  }

  /**
   * Date in "ago" format.
   *
   * Twitter displays all tweets that are less than 24 hours old with something
   * like "about 4 hours ago" and ones older than 24 hours with a time and date.
   * This function allows us to simulate that functionality, but lets us choose
   * where the dividing line is.
   *
   * @param int $start_timestamp
   *   The timestamp used to calculate time passed
   * 
   * @return string
   *   Time in "ago" format
   */
  protected function timeSince($start_timestamp) {
    $chunks = array(
      'year'   => 60 * 60 * 24 * 365,
      'month'  => 60 * 60 * 24 * 30,
      'week'   => 60 * 60 * 24 * 7,
      'day'    => 60 * 60 * 24,
      'hour'   => 60 * 60,
      'minute' => 60,
      'second' => 1,
    );

    $since = time() - $start_timestamp;

    foreach ($chunks as $key => $seconds) {
      if (($count = floor($since / $seconds)) != 0) {
        break;
      }
    }

    $messages = array(
      'year'   => t('@n years ago', array('@n' => $count)),
      'month'  => t('@n months ago', array('@n' => $count)),
      'week'   => t('@n weeks ago', array('@n' => $count)),
      'day'    => t('@n days ago', array('@n' => $count)),
      'hour'   => t('@n hours ago', array('@n' => $count)),
      'minute' => t('@n minutes ago', array('@n' => $count)),
      'second' => t('@n seconds ago', array('@n' => $count)),
    );

    return sprintf($messages[$key], $count);
  }

  /**
   * Make date string based on format.
   */
  public function processDate($date, $params) {
    $date = strtotime($date);
    if ($params['date_style']) {
      return $this->timeSince($date);
    }
    if (!empty($params['date_format'])) {
      return date($params['date_format'], $date);
    }

    return date('d F Y', $date);
  }
}


class DrupalTwitterException extends Exception {}
/**
 * Twitter OAuth class
 */
class DrupalTwitterOAuth {
  /* Contains the last HTTP status code returned. */
  public $httpCode;
  /* Contains the last API call. */
  public $url;
  /* Set up the API root URL. */
  public $host = "https://api.twitter.com/1/";
  /* Set timeout default. */
  public $timeout = 30;
  /* Set connect timeout. */
  public $connecttimeout = 30;
  /* Verify SSL Cert. */
  public $sslVerifypeer = FALSE;
  /* Respons format. */
  public $format = 'json';
  /* Decode returned json data. */
  public $decodeJSON = TRUE;
  /* Contains the last HTTP headers returned. */
  public $httpInfo;
  /* Set the useragnet. */
  public $useragent = 'TwitterOAuth v0.2.0-beta2';

  /**
   * Set Token URL.
   */
  public function accessTokenURL() {return 'https://api.twitter.com/oauth/access_token';}

  /**
   * Set Authenticate URL.
   */
  public function authenticateURL() {return 'https://api.twitter.com/oauth/authenticate';}

  /**
   * Set Authorize URL.
   */
  public function authorizeURL() {return 'https://api.twitter.com/oauth/authorize';}

  /**
   * Set Request URL.
   */
  public function requestTokenURL() {return 'https://api.twitter.com/oauth/request_token';}

  /**
   * Get HTTP Status Code.
   */
  public function lastStatusCode() {return $this->http_status;}

  /**
   * Get last API call.
   */
  public function lastAPICall() {return $this->last_api_call;}

  /**
   * Construct TwitterOAuth object.
   */
  public function __construct($consumer_key, $consumer_secret, $oauth_token = NULL, $oauth_token_secret = NULL) {
    $this->sha1_method = new OAuthSignatureMethod_HMAC_SHA1();
    $this->consumer = new OAuthConsumer($consumer_key, $consumer_secret);
    if (!empty($oauth_token) && !empty($oauth_token_secret)) {
      $this->token = new OAuthConsumer($oauth_token, $oauth_token_secret);
    }
    else {
      $this->token = NULL;
    }
  }


  /**
   * Get a request_token from Twitter.
   *
   * @returns a key/value array containing oauth_token and oauth_token_secret
   */
  public function getRequestToken($oauth_callback = NULL) {
    $parameters = array();
    if (!empty($oauth_callback)) {
      $parameters['oauth_callback'] = $oauth_callback;
    }
    $request = $this->oAuthRequest($this->requestTokenURL(), 'GET', $parameters);
    $token = OAuthUtil::parse_parameters($request);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  /**
   * Get the authorize URL.
   *
   * @returns a string
   */
  public function getAuthorizeURL($token, $sign_in_with_twitter = TRUE) {
    if (is_array($token)) {
      $token = $token['oauth_token'];
    }
    if (empty($sign_in_with_twitter)) {
      return $this->authorizeURL() . "?oauth_token={$token}";
    }
    else {
      return $this->authenticateURL() . "?oauth_token={$token}";
    }
  }

  /**
   * Get access token.
   *
   * @returns array("oauth_token" => "the-access-token",
   *                "oauth_token_secret" => "the-access-secret",
   *                "user_id" => "9436992",
   *                "screen_name" => "abraham")
   */
  public function getAccessToken($oauth_verifier = FALSE) {
    $parameters = array();
    if (!empty($oauth_verifier)) {
      $parameters['oauth_verifier'] = $oauth_verifier;
    }
    $request = $this->oAuthRequest($this->accessTokenURL(), 'GET', $parameters);
    $token = OAuthUtil::parse_parameters($request);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  /**
   * One time exchange of username and password for access token and secret.
   *
   * @returns array("oauth_token" => "the-access-token",
   *                "oauth_token_secret" => "the-access-secret",
   *                "user_id" => "9436992",
   *                "screen_name" => "abraham",
   *                "x_auth_expires" => "0")
   */
  public function getXAuthToken($username, $password) {
    $parameters = array();
    $parameters['x_auth_username'] = $username;
    $parameters['x_auth_password'] = $password;
    $parameters['x_auth_mode'] = 'client_auth';
    $request = $this->oAuthRequest($this->accessTokenURL(), 'POST', $parameters);
    $token = OAuthUtil::parse_parameters($request);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  /**
   * GET wrapper for oAuthRequest.
   */
  public function get($url, $parameters = array()) {
    $response = $this->oAuthRequest($url, 'GET', $parameters);
    if ($this->format === 'json' && $this->decodeJSON) {
      return json_decode($response);
    }
    return $response;
  }

  /**
   * POST wrapper for oAuthRequest.
   */
  public function post($url, $parameters = array()) {
    $response = $this->oAuthRequest($url, 'POST', $parameters);
    if ($this->format === 'json' && $this->decodeJSON) {
      return json_decode($response);
    }
    return $response;
  }

  /**
   * DELETE wrapper for oAuthReqeust.
   */
  public function delete($url, $parameters = array()) {
    $response = $this->oAuthRequest($url, 'DELETE', $parameters);
    if ($this->format === 'json' && $this->decodeJSON) {
      return json_decode($response);
    }
    return $response;
  }

  /**
   * Format and sign an OAuth / API request.
   */
  public function oAuthRequest($url, $method, $parameters) {
    if (strrpos($url, 'https://') !== 0 && strrpos($url, 'http://') !== 0) {
      $url = "{$this->host}{$url}.{$this->format}";
    }
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->token, $method, $url, $parameters);
    $request->sign_request($this->sha1_method, $this->consumer, $this->token);
    switch ($method) {
      case 'GET':
        return $this->http($request->to_url(), 'GET');

      default:
        return $this->http($request->get_normalized_http_url(), $method, $request->to_postdata());
    }
  }

  /**
   * Make an HTTP request.
   */
  public function http($url, $method, $postfields = NULL) {
    $this->httpInfo = array();
    $ci = curl_init();
    /* Curl settings */
    curl_setopt($ci, CURLOPT_USERAGENT, $this->useragent);
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, $this->connecttimeout);
    curl_setopt($ci, CURLOPT_TIMEOUT, $this->timeout);
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ci, CURLOPT_HTTPHEADER, array('Expect:'));
    curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, $this->sslVerifypeer);
    curl_setopt($ci, CURLOPT_HEADERFUNCTION, array($this, 'getHeader'));
    curl_setopt($ci, CURLOPT_HEADER, FALSE);

    switch ($method) {
      case 'POST':
        curl_setopt($ci, CURLOPT_POST, TRUE);
        if (!empty($postfields)) {
          curl_setopt($ci, CURLOPT_POSTFIELDS, $postfields);
        }
        break;

      case 'DELETE':
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'DELETE');
        if (!empty($postfields)) {
          $url = "{$url}?{$postfields}";
        }
    }

    curl_setopt($ci, CURLOPT_URL, $url);
    $response = curl_exec($ci);
    $this->httpCode = curl_getinfo($ci, CURLINFO_HTTP_CODE);
    $this->httpInfo = array_merge($this->httpInfo, curl_getinfo($ci));
    $this->url = $url;
    curl_close($ci);
    return $response;
  }

  /**
   * Get the header info to store.
   */
  public function getHeader($ch, $header) {
    $i = strpos($header, ':');
    if (!empty($i)) {
      $key = str_replace('-', '_', strtolower(substr($header, 0, $i)));
      $value = trim(substr($header, $i + 2));
      $this->http_header[$key] = $value;
    }
    return strlen($header);
  }
}
