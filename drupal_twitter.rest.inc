<?php

/**
 * @file
 * Twitter rest API implementation.
 */

module_load_include('inc', 'drupal_twitter', 'drupal_twitter.core');

class DrupalTwitterAPI extends DrupalTwitterItem {

  protected $protocol = 'https://';
  protected $consumerKey;
  protected $consumerSecret;
  protected $accessKey;
  protected $accessSecret;

  /**
   * Constructor.
   */
  public function __construct($params = array()) {
    parent::__construct($params);

    $this->consumerKey = variable_get('drupal_twitter_consumer_key', NULL);
    $this->consumerSecret = variable_get('drupal_twitter_consumer_secret', NULL);
    $this->accessKey = variable_get('drupal_twitter_access_key', NULL);
    $this->accessSecret = variable_get('drupal_twitter_access_secret', NULL);
  }

  /**
   * Creates an API endpoint URL.
   *
   * @param string $path
   *   The path of the endpoint.
   * @param string $format
   *   The format of the endpoint to be appended at the end of the path.
   *
   * @return string
   *   The complete path to the endpoint.
   */
  protected function createURL($path, $format = '.json') {
    $url = DRUPAL_TWITTER_API . '/1.1/' . $path . $format;
    return $url;
  }

  /**
   * Main request function.
   *
   * @param string $url
   *   The url to request
   * @param array $params
   *   Additional parameters to pass with request
   *
   * @return array
   *   Array of tweet's objects
   */
  protected function request($url, $params = array()) {
    $response = array();
    try {
      $connection = new DrupalTwitterOAuth($this->consumerKey, $this->consumerSecret, $this->accessKey, $this->accessSecret);
      $response = $connection->get($url, $params);
    }
    catch (DrupalTwitterException $e){
      drupal_set_message(t('Problem in request: ') . $e->getMessage());
    }
    if (is_object($response)) {
      $response = isset($response->statuses) ? $response->statuses : array();
    }
    if (is_array($response) && count($response)) {
      foreach ($response as $item) {
        $resp[$item->id_str] = $item;
      }
      $response = $resp;
    }
    return $response;
  }

  /**
   * Helper function to check user availability.
   *
   * @param string $user
   *   User screen_name to check
   *
   * @return array
   *   Array with status message and request code
   */
  public function checkUser($user) {
    try {
      $connection = new DrupalTwitterOAuth($this->consumerKey, $this->consumerSecret, $this->accessKey, $this->accessSecret);
      $resp = $connection->get('users/lookup', array('screen_name' => $user));
      return array('status' => '', 'code' => $connection->httpCode);
    }
    catch (DrupalTwitterException $e){
      drupal_set_message(t('Error accured: ') . $e->getMessage(), 'error');
    }
  }

  /**
   * Gets tweets.
   *
   * @return array
   *   Tweets will be passed to block template
   */
  public function getItems() {
    $tweets = array();
    if (isset($this->users) && !isset($this->tags) && !isset($this->search)) {
      $url = $this->createURL('statuses/user_timeline');
      foreach ($this->users as $user) {
        if (empty($user)) {
          continue;
        }
        $params['screen_name'] = urlencode($user);
        $result = $this->request($url, $params);
        $tweets += $result;
      }
    }
    else {
      $params = array('count' => 100);
      $url = $this->createURL('search/tweets');
      $data = array();
      if (isset($this->users)) {
        $data['u'] = $this->buildUsersQuery($this->users);
      }
      if (isset($this->tags)) {
        $data['t'] = $this->buildTagsQuery($this->tags);
      }
      if (isset($this->search)) {
        $data['s'] = urlencode($this->search);
      }
      $params['q'] = implode('+', $data);
      $tweets = $this->request($url, $params);
    }
    if (count($tweets)) {
      krsort($tweets);
      $tweets = $this->processFeed($tweets);
    }
    return $tweets;
  }

  /**
   * We have to return tweets in format of associative array.
   *
   * @param string $data
   *   json decoded data from twitter feed
   *
   * @return array
   *   array of items
   */
  public function processFeed($data) {
    $items = array();
    $num = $this->params['num'];
    $data = array_slice($data, 0, $num, 1);

    foreach ($data as $key => $item) {
      $items[$key]['id'] = $item->id_str;
      if ($this->params['clickable']) {
        $item->text = $this->linkTwitterUsers($item->text);
        $item->text = $this->linkHashtags($item->text);
        $item->text = $this->linkUrls($item->text);
      }
      $items[$key]['tweet'] = $item->text;
      $items[$key]['source'] = $item->source;
      $items[$key]['date'] = $this->processDate($item->created_at, $this->params);
      if (isset($item->user)) {
        $items[$key]['user'] = $item->user->screen_name;
      }
      else {
        $items[$key]['user'] = $item->from_user;
      }
      $items[$key]['url'] = $this->linkToTweet($items[$key]['user'], $item->id_str);
      $items[$key]['link'] = $this->linkToProfile($items[$key]['user']);
      if ($image = $this->processAvatar($items[$key]['user'])) {
        $items[$key]['avatar'] = $image;
      }
    }
    return $items;
  }

  /**
   * Get profile image.
   */
  public function processAvatar($user) {
    return DrupalTwitter::getProfileImage($user, $this->params['avatar_style']);
  }
}
