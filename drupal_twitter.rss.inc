<?php

/**
 * @file
 * Twitter RSS API implementation.
 */

module_load_include('inc', 'drupal_twitter', 'drupal_twitter.core');

class DrupalTwitterRSS extends DrupalTwitterItem {

  /**
   * Constructor.
   */
  public function __construct($params = array()) {
    parent::__construct($params);
  }

  /**
   * Gets tweets.
   *
   * @return array
   *   Tweets will be passed to block template
   */
  public function getItems() {
    $tweets = array();
    if (isset($this->users) && !isset($this->tags) && !isset($this->search)) {
      $url = $this->protocol . $this->apiURL . 'statuses/user_timeline.json';
      foreach ($this->users as $user) {
        if (empty($user)) {
          continue;
        }
        $url_req = $url . '?screen_name=' . urlencode($user);
        $result = $this->request($url_req);
        $tweets += $result;
      }
    }
    else {
      $url = $this->protocol . $this->searchURL;
      $data = array();
      if (isset($this->users)) {
        $data['u'] = $this->buildUsersQuery($this->users);
      }

      if (isset($this->tags)) {
        $data['t'] = $this->buildTagsQuery($this->tags);
      }

      if (isset($this->search)) {
        $data['s'] = urlencode($this->search);
      }
      @$url_req = $url . implode('+', $data);
      $tweets = $this->request($url_req, 1);
    }
    if (count($tweets)) {
      krsort($tweets);
      $tweets = $this->processFeed($tweets);
    }
    return $tweets;
  }


  /**
   * Gets JSON feed from Twitter and returns an array of objects.
   *
   * @return array
   *   array of items
   */
  protected function request($url, $search = FALSE) {
    $tweets = array();
    $result = drupal_http_request($url);
    if ($result->code == 200) {
      $data = json_decode($result->data);
      if (isset($data->results)) {
        foreach ($data->results as $item) {
          $tweets[$item->id_str] = $item;
        }
      }

      if (is_array($data) && count($data)) {
        foreach ($data as $item) {
          $tweets[$item->id_str] = $item;
        }
        return $tweets;
      }
    }
    return $tweets;
  }

  /**
   * Helper function to check user availability.
   *
   * @param string $user
   *   User screen_name to check
   *
   * @return array
   *   Array with status message and request code
   */
  public function checkUser($user) {
    $this->normalize($user);
    if (strlen(trim($user)) == 0) {
      return array();
    }
    $req = drupal_http_request($this->protocol . $this->apiURL . 'statuses/user_timeline.json?screen_name=' . $user);
    return array('status' => $req->status_message, 'code' => $req->code);
  }

  /**
   * We have to return tweets as associative array.
   *
   * @param string $data
   *   json decoded data from twitter feed
   * 
   * @return array
   *   array of items
   */
  public function processFeed($data) {
    $items = array();
    $num = $this->params['num'];
    $data = array_slice($data, 0, $num, 1);

    foreach ($data as $key => $item) {
      $items[$key]['id'] = $item->id_str;
      if ($this->params['clickable']) {
        $item->text = $this->linkTwitterUsers($item->text);
        $item->text = $this->linkHashtags($item->text);
        $item->text = $this->linkUrls($item->text);

      }
      $items[$key]['tweet']       = $item->text;
      $items[$key]['source']      = $item->source;
      $items[$key]['date']        = $this->processDate($item->created_at, $this->params);
      if (isset($item->user)) {
        $items[$key]['user']        = $item->user->screen_name;
      }
      else {
        $items[$key]['user']        = $item->from_user;
      }
      $items[$key]['url'] = $this->linkToTweet($items[$key]['user'], $item->id_str);
      $items[$key]['link'] = $this->linkToProfile($items[$key]['user']);
      if ($image = $this->processAvatar($items[$key]['user'])) {
        $items[$key]['avatar'] = $image;
      }

    }
    return $items;
  }

  /**
   * Get profile image.
   */
  public function processAvatar($user) {
    return DrupalTwitter::getProfileImage($user, $this->params['avatar_style']);
  }
}
